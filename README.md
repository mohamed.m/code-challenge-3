# Intermediate Angular Developer Challenge

### Introduction
[Angular Material](https://material.angular.io/) is Material Design components for Angular which have 
a vast UI components that follows [Google Material Guidelines](https://material.io/design/). One of the most commonly used component and even existed before Angular and Material design is DataTable. DataTable is UI component that displays data in table layout with the ability to sort, filter, and many other actions on the table's data.

### Goal
The goal of this challenge is to create a DataTable using Angular Material. You can refer to the component documentation [here](https://material.angular.io/components/table/overview). Show us a useful example of your DataTable which includes at **least ID/Serial, Title, and Image**.


| ID 	| TITLE                                                    	| Image                                                                	|
|----	|----------------------------------------------------------	|----------------------------------------------------------------------	|
| 1 	| Lorem ipsum dolor sit amet, consectetur adipiscing elit. 	| ![Sample Image](http://via.placeholder.com/100x100) 	|


The table will fetch the data from RESTful API you can use any of the following free services to get your API endpoints:-
* [JSON: Placeholder](https://jsonplaceholder.typicode.com/)
* [AnyAPI](https://any-api.com/)


### Criteria of Acceptence
* Well organized and documented code.
* Creating a service for handling API requests.
* Apply Material DataTable Paginator [Documentation Refrence](https://material.angular.io/components/paginator/overview)
* Apply Table Header Sorting [Documentation Refrence](https://material.angular.io/components/sort/overview)

### Bonus
* Create a search field to filter table results. 
* Create a test.
* Use SCSS. 
* Create a [StackBlitz](https://stackblitz.com/) demo.